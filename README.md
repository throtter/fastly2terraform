# fastly2terraform.py

#fastly #terraform #idgus


Automatically generate terraform configuration from fastly api.

## Not implemented

- acls
- dictionaries
- directors
- response_objects
- snippets
- wordpress

as they are not used at the moment.

## Implemented

- domain
- backend
- vcl
- cache_setting
- condition
- gzip
- header
- healthcheck
- request_setting
- s3logging

## Prerequisites

- Python3

```
brew install python3
```

- terraform

```
brew install terraform
```

- jinja2
- requests

```
pip3 install jinja2
pip3 install requests
```



## Usage

- General

```
./fastly2terraform.py --api_key=<api_key> --service_id=<service_id>
```

- Example: apollo_prod_jvw

```
./fastly2terraform.py --api_key=<api_key> --service_id='4B1BdxKGzS2ilNYyyJ2zVy'
```

- Example: testhive3

```
./fastly2terraform.py --api_key=<api_key> --service_id='0zG06K1ld9ceymGaTq9Mrw'
```


## Result

For apollo_prod_jvw & testhive3:

```
├── 0zG06K1ld9ceymGaTq9Mrw.json (Cache - delete to renew from API)
├── 4B1BdxKGzS2ilNYyyJ2zVy.json (Cache - delete to renew from API)
├── apollo_prod_jvw (VCLs)
│   ├── acl.vcl
│   ├── argument_whitelist.vcl
│   ├── cookie_redirect.vcl
│   ├── custom_error.vcl
│   ├── logging.vcl
│   ├── main.vcl
│   ├── no_cache.vcl
│   ├── redirects.vcl
│   ├── resource.vcl
│   └── ttl.vcl
├── apollo_prod_jvw.tf
├── config.tf
├── fastly2terraform.py
├── terraform.tfvars
├── testhive3  (VCLs)
│   ├── acl.vcl
│   ├── argument_whitelist.vcl
│   ├── cookie_redirect.vcl
│   ├── custom_error.vcl
│   ├── locales.vcl
│   ├── logging.vcl
│   ├── main.vcl
│   ├── no_cache.vcl
│   ├── redirect_testhive.vcl
│   ├── redirects.vcl
│   ├── resource.vcl
│   └── ttl.vcl
├── testhive3.tf
└── variables.tf
```