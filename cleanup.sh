#!/bin/bash

find . -type d -not -path '.' -not -path '*/.*' -maxdepth 1 -exec rm -Rf {} \;
find . -type f -name '*.tf*' -maxdepth 1 -exec rm -f {} \;
find . -type f -name '*.json' -maxdepth 1 -exec rm -f {} \;
