#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json
import os.path
from jinja2 import Template
import sys
import getopt


def getConfiguration(api_key, service_id):
    filename = '{}.json'.format(service_id)
    if os.path.isfile(filename):
        fo = open(filename, 'r')
        config_json = json.load(fo)
        fo.close()
    else:
        url = 'https://api.fastly.com/service/{}/details'.format(service_id)
        headers = {'Fastly-Key': api_key,
                   'Accept': 'application/json'}
        r = requests.get(url=url, headers=headers)
        config_json = r.json()
        fo = open(filename, 'wb')
        fo.write(json.dumps(config_json).encode('utf-8'))
        fo.close()

    return config_json


def createVcls(configuration):
    if not os.path.isdir(configuration['name']):
        os.mkdir(configuration['name'])

    for vcl in configuration['active_version']['vcls']:
        fo = open(os.path.join(configuration['name'], vcl['name']), 'wb')
        fo.write(vcl['content'].encode('utf-8'))
        fo.close()


def createTerraform(configuration, api_key):
    # variables
    template = Template('''
variable "api_key" {
  type        = "string"
  description = "Fastly API Key"
}
''')
    rendered_template = template.render()
    print(rendered_template)

    terraform_file = 'variables.tf'
    fo = open(terraform_file, 'wb')
    fo.write(rendered_template.encode('utf-8'))
    fo.close()
    os.system('terraform fmt {}'.format(terraform_file))

    # tfvars
    template = Template('''
api_key = "{{api_key}}"
''')
    rendered_template = template.render(api_key=api_key)
    print(rendered_template)

    terraform_file = 'terraform.tfvars'
    fo = open(terraform_file, 'wb')
    fo.write(rendered_template.encode('utf-8'))
    fo.close()
    os.system('terraform fmt {}'.format(terraform_file))

    # config
    template = Template('''
terraform {
  required_version = ">= 0.10.3"

  # backend "s3" {
  #   bucket = "idg-tfstate"
  #   key    = "fastly"
  #   region = "eu-central-1"
  # }
}

provider "fastly" {
  api_key = "${var.api_key}"
}
''')
    rendered_template = template.render()
    print(rendered_template)

    terraform_file = 'config.tf'
    fo = open(terraform_file, 'wb')
    fo.write(rendered_template.encode('utf-8'))
    fo.close()
    os.system('terraform fmt {}'.format(terraform_file))

    # service
    template = Template('''
resource "fastly_service_v1" "{{configuration['name']}}" {
  name          = "{{configuration['name']}}"
  force_destroy = true

  default_host = "{{configuration['active_version']['settings']['general.default_host']}}"
  default_ttl  = "{{configuration['active_version']['settings']['general.default_ttl']}}"

  {% for domain in configuration['active_version']['domains'] %}
  domain {
    name    = "{{domain.name}}"
    comment = "{{domain.comment}}"
  }
  {% endfor %}

  {% for backend in configuration['active_version']['backends'] %}
  backend {
    name                  = "{{backend.name}}"
    address               = "{{backend.address}}"
    auto_loadbalance      = {{backend.auto_loadbalance|lower}}
    between_bytes_timeout = {{backend.between_bytes_timeout}}
    connect_timeout       = {{backend.connect_timeout}}
    error_threshold       = {{backend.error_threshold}}
    first_byte_timeout    = {{backend.first_byte_timeout}}
    max_conn              = {{backend.max_conn}}
    port                  = {{backend.port}}
    request_condition     = "{{backend.request_condition}}"
    use_ssl               = {{backend.use_ssl|lower}}
    max_tls_version       = "{{backend.max_tls_version|default('', true)}}"
    min_tls_version       = "{{backend.min_tls_version|default('', true)}}"
    ssl_ciphers           = "{{backend.ssl_ciphers|default('', true)}}"
    ssl_ca_cert           = "{{backend.ssl_ca_cert|default('', true)}}"
    ssl_client_cert       = "{{backend.ssl_client_cert|default('', true)}}"
    ssl_client_key        = "{{backend.ssl_client_key|default('', true)}}"
    ssl_check_cert        = {{backend.ssl_check_cert|lower}}
    # ssl_hostname          = "{{backend.ssl_hostname|default('', true)}}"
    ssl_cert_hostname     = "{{backend.ssl_hostname|default('', true)}}"
    ssl_sni_hostname      = "{{backend.ssl_hostname|default('', true)}}"
    ssl_cert_hostname     = "{{backend.ssl_cert_hostname|default('', true)}}"
    ssl_sni_hostname      = "{{backend.ssl_sni_hostname|default('', true)}}"
    {%- if backend.shield != None %}
    shield                = "{{backend.shield}}"
    {%- endif %}
    weight                = {{backend.weight}}
    {%- if backend.healthcheck != None %}
    healthcheck           = "{{backend.healthcheck}}"
    {%- endif %}
  }
  {% endfor %}

  {% for vcl in configuration['active_version']['vcls'] %}
  vcl {
    name    = "{{vcl.name}}"
    content = "${file("./{{configuration['name']}}/{{vcl.name}}")}"
    main    = {{vcl.main|lower}}
  }
  {% endfor %}

  {% for cache_setting in configuration['active_version']['cache_settings'] %}
  cache_setting {
    name            = "{{cache_setting.name}}"
    action          = "{{cache_setting.action}}"
    cache_condition = "{{cache_setting.cache_condition}}"
    stale_ttl       = {{cache_setting.stale_ttl|default('0', true)}}
    ttl             = {{cache_setting.ttl|default('0', true)}}
  }
  {% endfor %}

  {% for condition in configuration['active_version']['conditions'] %}
  condition {
    name      = "{{condition.name}}"
    statement = "{{condition.statement|replace('\\\\', '\\\\\\\\')|replace('"', '\\\\"')}}"
    type      = "{{condition.type}}"
    priority  = {{condition.priority}}
  }
  {% endfor %}

  {% for gzip in configuration['active_version']['gzips'] %}
  gzip {
    name            = "{{gzip.name}}"
    {%- set content_types = gzip.content_types.split(' ') -%}
    content_types   = ["{{content_types|join('","')}}"]
    {%- set extensions = gzip.extensions.split(' ') -%}
    extensions      = ["{{extensions|join('","')}}"]
    cache_condition = "{{gzip.cache_condition}}"
  }
  {% endfor %}

  {% for header in configuration['active_version']['headers'] %}
  header {
    name               = "{{header.name}}"
    action             = "{{header.action}}"
    type               = "{{header.type}}"
    destination        = "{{header.dst}}"
    ignore_if_set      = {{header.ignore_if_set|replace('0', 'false')|replace('1', 'true')}}
    {%- if header.src != None %}
    source             = "{{header.src|replace('"', '\\\\"')}}"
    {%- endif %}
    regex              = "{{header.regex}}"
    substitution       = "{{header.substitution}}"
    priority           = {{header.priority}}
    request_condition  = "{{header.request_condition|default('', true)}}"
    cache_condition    = "{{header.cache_condition|default('', true)}}"
    response_condition = "{{header.response_condition|default('', true)}}"
  }
  {% endfor %}

  {% for healthcheck in configuration['active_version']['healthchecks'] %}
  healthcheck {
    name              = "{{healthcheck.name}}"
    host              = "{{healthcheck.host}}"
    path              = "{{healthcheck.path}}"
    check_interval    = {{healthcheck.check_interval}}
    expected_response = {{healthcheck.expected_response}}
    http_version      = "{{healthcheck.http_version}}"
    initial           = {{healthcheck.initial}}
    method            = "{{healthcheck.method}}"
    threshold         = {{healthcheck.threshold}}
    timeout           = {{healthcheck.timeout}}
    window            = {{healthcheck.window}}
  }
  {% endfor %}

  {% for request_setting in configuration['active_version']['request_settings'] %}
  request_setting {
    name              = "{{request_setting.name}}"
    request_condition = "{{request_setting.request_condition}}"
    max_stale_age     = "{{request_setting.max_stale_age}}"
    force_miss        = "{{request_setting.force_miss|default('', true)}}"
    force_ssl         = "{{request_setting.force_ssl|default('', true)}}"
    action            = "{{request_setting.action}}"
    bypass_busy_wait  = "{{request_setting.bypass_busy_wait|default('', true)}}"
    {%- if request_setting.hash_keys != None %}
    hash_keys         = "{{request_setting.hash_keys}}"
    {%- endif %}
    xff               = "{{request_setting.xff}}"
    timer_support     = "{{request_setting.timer_support|default('', true)}}"
    geo_headers       = "{{request_setting.geo_headers|default('', true)}}"
    {%- if request_setting.default_host != None and request_setting.default_host != "None" %}
    default_host      = "{{request_setting.default_host}}"
    {%- endif %}
  }
  {% endfor %}

  {% for s3 in configuration['active_version']['s3s'] %}
  s3logging {
    name               = "{{s3.name}}"
    bucket_name        = "{{s3.bucket_name}}"
    s3_access_key      = "{{s3.s3_access_key}}"
    s3_secret_key      = "{{s3.s3_secret_key}}"
    {%- if s3.path != None and s3.path != "None" %}
    path               = "{{s3.path}}"
    {%- endif %}
    domain             = "{{s3.domain}}"
    period             = {{s3.period}}
    gzip_level         = {{s3.gzip_level}}
    format             = "{{s3.format}}"
    format_version     = "{{s3.format_version}}"
    message_type       = "{{s3.message_type}}"
    {%- if s3.redundancy != None %}
    redundancy         = "{{s3.redundancy}}"
    {%- endif %}
    response_condition = "{{s3.response_condition}}"
    placement          = "{{s3.placement|default('none', true)}}"
    # timestamp_format   = "{{s3.timestamp_format}}"
  }
  {% endfor %}
}''')

    rendered_template = template.render(configuration=configuration)
    print(rendered_template)

    terraform_file = '{}.tf'.format(configuration['name'])
    fo = open(terraform_file, 'wb')
    fo.write(rendered_template.encode('utf-8'))
    fo.close()
    os.system('terraform fmt {}'.format(terraform_file))


def main(argv):
    api_key = ''
    service_id = ''
    try:
        opts, args = getopt.getopt(argv, "ha:s:", ["api_key=", "service_id="])
    except getopt.GetoptError:
        print('fastly2terraform.py --api_key=<api_key> --service_id=<service_id>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('fastly2terraform.py --api_key=<api_key> --service_id=<service_id>')
            sys.exit()
        elif opt in ("-a", "--api_key"):
            api_key = arg
        elif opt in ("-s", "--service_id"):
            service_id = arg

    if len(api_key) == 0 or len(service_id) == 0:
        print('fastly2terraform.py --api_key=<api_key> --service_id=<service_id>')
        sys.exit(2)

    configuration = getConfiguration(api_key=api_key, service_id=service_id)
    createVcls(configuration=configuration)
    createTerraform(configuration=configuration, api_key=api_key)

if __name__ == '__main__':
    main(sys.argv[1:])
